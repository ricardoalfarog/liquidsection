# coding: utf-8

# Change Compass configuration
compass_config do |config|
  # routes for stylesheets directories at build mode
  config.sass_options = {:output_style => :nested, :images_dir => 'images', :fonts_dir => 'fonts'}
end

Encoding.default_external = 'utf-8'

# for physical directories at development mode
set :images_dir,  "images"
set :fonts_dir,  "fonts"
set :css_dir,  "stylesheets"
set :js_dir, "javascripts"
set :build_dir, "build"


#set :cache_file_name, "rhyboo.appcache"
#set :default_stage_config, "rhyboo_development"
#set :env_file_path, "assets/javascripts/common/env.js"
#set :custom_css_file_path, "assets/stylesheets/custominterface.css"

set :markdown, :layout_engine => :haml
set :default_encoding, 'utf-8'
set :relative_links, false

# load extensions
#require "extensions/smusher"
#require "extensions/outdate_cache"
#require "extensions/config_env"

# Build-specific configuration

configure :build do
  #activate :compass

  # Outdate the cache manifest
  #activate :outdate_cache
  #activate :config_env

  # minify and gzip must run after config_env
  #activate :minify_css
  #activate :minify_javascript
  #activate :gzip

  # Use relative URLs
  activate :relative_assets

  # Enable cache buster
  # activate :cache_buster

  # Compress PNGs after build
  #activate :smusher

  # Or use a different image path
  #set :http_path, "./"
end




###
# Haml
###

## Haml to output unindented text

# CodeRay syntax highlighting in Haml
# First: gem install haml-coderay
# require 'haml-coderay'

# CoffeeScript filters in Haml
# First: gem install coffee-filter
# require 'coffee-filter'

# Automatic image dimensions on image_tag helper
# activate :automatic_image_sizes




###
# Page command
###

# Per-page layout changes:
#
# With no layout
# page "/loadtest.html", :layout => false
# page "/feedback.html", :layout => false
#
# With alternative layout
# page "/path/to/file.html", :layout => :otherlayout
# page "/path/to/file.html", :layout => :otherlayout
# page "/templates/*", :layout => "/templates"


#
# A path which all have the same layout
# with_layout :admin do
#   page "/admin/*"
# end

# Proxy (fake) files
# page "/this-page-has-no-template.html", :proxy => "/template-file.html" do
#   @which_fake_page = "Rendering a fake page with a variable"
# end




###
# Helpers
###

# Methods defined in the helpers block are available in templates
#helpers do
#  def template(t)
#    partial "partials/#{t}"
#  end
#end





